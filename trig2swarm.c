/*
 *   $Id: trig2swarm.c
 *
 *    Revision history:
 *
 *     Revision 1.0  2022/07/18 victor preatoni
 *     Initial revision. Based on ew2file
 *
 */

/*
 *   trig2swarm.c
 *
 *   Program to read TYPE_TRIGLIST_SCNL messages from a ring
 *   and write them to a CSV using Swarm tagging format:
 *
 * YYYY-mm-dd HH:mm:ss      SSSS CCC NN LL      tag
 *
 * Where:
 *      YYYY    4 digit year
 *      mm      2 digit month
 *      dd      2 digit day
 *      HH      2 digit 24-hour
 *      MM      2 digit minute
 *      ss      2 digit seconds
 *      SSSS    Station
 *      CCC     Channel
 *      NN      Network
 *      LL      Location
 *      tag     Custom tag for triggered station
 *
 * Eg: 2022-04-07 11:13:02,BARZ HHZ TC PP,EW_TRIGGER
 *
 *   Victor Preatoni | OAVV-SEGEMAR | 2022-07-18
 *
 */

#define TRIG2SWARM_VERSION "1.0.2 - Aug 10, 2022"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <time.h>
#include <time_ew.h>
#include <errno.h>
#include <signal.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <mem_circ_queue.h>
#include <read_arc.h>

#ifdef _LINUX
#include <sys/param.h> 	/* needed for MAXPATHLEN */
#endif

#ifdef _MACOSX
#include <sys/param.h> 	/* needed for MAXPATHLEN */
#endif

#ifndef MAXPATHLEN
#define MAXPATHLEN 256
#endif

#define MAX_STR 512


/* Functions in this source file
*******************************/
typedef struct {
        char dateStr[11];
        char timeStr[9];
        char s[6];
        char c[4];
        char n[3];
        char l[3];
        char tag[32];
} TrigLine;


void trig2swarm_config(char *);
void trig2swarm_lookup(void);
void trig2swarm_status(MSG_LOGO *, char *);
int writeFile(char *msg, size_t msglen, char *dir, char *filename, char *errText);
int buildDateString(struct tm dateTime, char *tstring);
int buildTimeString(struct tm dateTime, char *tstring);
int buildTrigString(TrigLine trig, char *buffer);
char * matchDate(char *msg);
char * matchTime(char *msg);
char * matchDateTime(char *msg);
void getDate(char *msg, struct tm *dateTime);
void getTime(char *msg, struct tm *dateTime);
struct tm getDateTime(char *msg);
int getNextTrigger(char *msg, char *line);
int parseTrigger(char *line, TrigLine *trig);



/* Thread things
***************/
#define THREAD_STACK 8192
static ew_thread_t tidDup;              /* Dup thread id */
static ew_thread_t tidStacker;          /* Thread moving messages from transport */
                                     /*   to queue */

#define MSGSTK_OFF    0              /* MessageStacker has not been started      */
#define MSGSTK_ALIVE  1              /* MessageStacker alive and well            */
#define MSGSTK_ERR   -1              /* MessageStacker encountered error quit    */
volatile int MessageStackerStatus = MSGSTK_OFF;

QUEUE OutQueue; 		     /* from queue.h, queue.c; sets up linked    */
                                     /*    list via malloc and free              */
thr_ret WriteThd( void * );
thr_ret MessageStacker( void * );    /* used to pass messages between main thread */
                                     /*   and Dup thread */
/* Message Buffers to be allocated
*********************************/
static char *Rdmsg = NULL;           /* msg retrieved from transport      */
static char *Wrmsg = NULL;           /*  message to write to file   */

/* Timers
******/
time_t MyLastBeat;            /* time of last local (into Earthworm) hearbeat */

char *cmdname;          /* pointer to executable name */
pid_t MyPid;		/* Our own pid, sent with heartbeat for restart purposes */

#define DEF_HEARTFILE_MSG "Alive\n"
#define DEF_HEARTFILE_SFX "hrt"

#define MAX_SERIAL     10000000
#define HEART_SERIAL    9999999
#define SERIAL_FORMAT  "%07d"

/* Things to read or derive from configuration file
**************************************************/
char      InRing[MAX_RING_STR];    /* name of transport ring for input  */
char      MyModName[MAX_MOD_STR];  /* speak as this module name/id      */
int       LogSwitch;        /* 0 if no logfile should be written        */
int       DebugSwitch;      /* 1 to log some debug info                 */
int       HeartBeatInt;     /* seconds between heartbeat messages       */
long      MaxMsgSize;       /* max size for input/output msgs           */
int       QueueSize;	    /* max messages in output circular buffer   */
SHM_INFO  InRegion;         /* shared memory region to use for input    */
char      *CSVDir;          /* destination directories                  */
char      *CSVSuffix;       /* CSV file extension                       */
char      *TriggeredTag;    /* Triggered station Swarm Tag              */
char      *CSVseparator;    /* CSV field separator                      */


/* Things to look up in the earthworm.h tables with getutil.c functions
**********************************************************************/
static long          InRingKey;     /* key of transport ring for input    */
static MSG_LOGO HeartLogo;          /* logo of heartbeat message          */
static MSG_LOGO ErrorLogo;          /* logo of error message              */
#define MAXLOGO   3
static MSG_LOGO GetLogo[MAXLOGO];   /* array for requesting module,type,instid */
static short 	  nLogo;            /* number of logos to get                   */
static unsigned char TypeError;
static unsigned char TypeTriglistSCNL;

/* Error messages used by trig2swarm
***********************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring        */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer      */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded  */
#define  ERR_QUEUE         3   /* queue error                             */
#define  ERR_WRITE         4   /* error writing message file              */

int main( int argc, char **argv )
{
    /* Other variables: */
    long     recsize;	/* size of retrieved message             */
    MSG_LOGO reclogo;	/* logo of retrieved message             */
    time_t   now;	/* current time, used for timing heartbeats */
    char     errText[256];     /* string for log/error/heartbeat messages           */

    /* Check command line arguments
     ******************************/
    cmdname = argv[0];
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <configfile>\n", cmdname);
        fprintf(stderr, "Version: %s\n", TRIG2SWARM_VERSION);
        return(0);
    }

    /* Initialize name of log-file & open it
     ****************************************/
    logit_init(argv[1], 0, 512, 1);

    /* Read the configuration file(s)
     ********************************/
    trig2swarm_config(argv[1]);
    logit("et", "%s(%s): Read command file <%s>\n", cmdname, MyModName, argv[1]);

    /* Look up important info from earthworm.h tables
     *************************************************/
    trig2swarm_lookup();

    /* Reinitialize the logging level
     *********************************/
    logit_init(argv[1], 0, 512, LogSwitch);

    /* Get our own Pid for restart purposes
     ***************************************/
    MyPid = getpid();
    if(MyPid == -1) {
        logit("e", "%s(%s): Cannot get pid; exiting!\n", cmdname, MyModName);
        return(0);
    }

    /* Allocate space for input/output messages for all threads
     ***********************************************************/
    /* Buffer for Read thread: */
    if ((Rdmsg = (char *)malloc(MaxMsgSize+1)) ==  NULL) {
        logit("e", "%s(%s): error allocating Rdmsg; exiting!\n", cmdname, MyModName);
        return(-1);
    }

    /* Buffers for Write thread: */
    if ((Wrmsg = (char *)malloc(MaxMsgSize+1)) ==  NULL) {
        logit("e", "%s(%s): error allocating Wrmsg; exiting!\n", cmdname, MyModName);
        return(-1);
    }

    /* Create a Mutex to control access to queue
     ********************************************/
    CreateMutex_ew();

    /* Initialize the message queue
     *******************************/
    initqueue(&OutQueue, (unsigned long)QueueSize, (unsigned long)(MaxMsgSize+1));

    /* Attach to Input/Output shared memory ring
     ********************************************/
    tport_attach(&InRegion, InRingKey);

    /* step over all messages from transport ring
     *********************************************/
    while (tport_getmsg(&InRegion, GetLogo, nLogo, &reclogo, &recsize, Rdmsg, MaxMsgSize) != GET_NONE);


    /* One heartbeat to announce ourselves to statmgr
     ************************************************/
    time(&MyLastBeat);
    sprintf(errText, "%ld %ld\n", (long)MyLastBeat, (long)MyPid);
    trig2swarm_status(&HeartLogo, errText);

    /* Start the message stacking thread if it isn't already running.
     ****************************************************************/
    if (MessageStackerStatus != MSGSTK_ALIVE) {
        if (StartThread(MessageStacker, (unsigned)THREAD_STACK, &tidStacker) == -1) {
            logit("e", "%s(%s): Error starting  MessageStacker thread; exiting!\n", cmdname, MyModName);
            tport_detach(&InRegion);
            exit(-1);
        }
        MessageStackerStatus = MSGSTK_ALIVE;
    }

    /* Start the file writing thread
     ***********************************/
    if (StartThread(WriteThd, (unsigned)THREAD_STACK, &tidDup) == -1) {
        logit("e", "%s(%s): Error starting Write thread; exiting!\n", cmdname, MyModName);
        tport_detach(&InRegion);
        exit(-1);
    }

    /* Start main trig2swarm service loop, which aimlessly beats its heart.
     **********************************/
    while (tport_getflag(&InRegion) != TERMINATE  && tport_getflag(&InRegion) != MyPid) {
        /* Beat the heart into the transport ring
            ****************************************/
        time(&now);
        if (difftime(now, MyLastBeat) > (double)HeartBeatInt) {
            sprintf(errText, "%ld %ld\n", (long)now, (long)MyPid);
            trig2swarm_status(&HeartLogo, errText);
            MyLastBeat = now;
        }

        /* take a brief nap; added 970624:ldd
            ************************************/
        sleep_ew(500);
    } /*end while of monitoring loop */

    /* Shut it down
     ***************/
    tport_detach( &InRegion );
    logit("t", "%s(%s): termination requested; exiting!\n", cmdname, MyModName);
    exit(0);
}
/* *******************  end of main *******************************
******************************************************************/



/**************************  Main Write Thread   ***********************
 *          Pull a messsage from the queue, and write it to a FILE!  *
 **********************************************************************/
thr_ret WriteThd(void *dummy )
{
	MSG_LOGO reclogo;
	int ret;
	int i;
	long msgSize;
	char errText[256];
	char filename[64];
    char triggerLine[128];
    struct tm dateTime;
    TrigLine triggerStruct;
    char write;
    
    
    while (1) {   /* main loop */
        /* Get message from queue
            *************************/
        RequestMutex();
        ret = dequeue(&OutQueue, Wrmsg, &msgSize, &reclogo);
        ReleaseMutex_ew();

        if (ret < 0) { /* -1 means empty queue */
            sleep_ew(500);
            continue;
        }

        /* Determine which GetLogo this message used */
        for (i = 0; i < nLogo; i++) {
            if ((GetLogo[i].type == WILD || GetLogo[i].type == reclogo.type) && (GetLogo[i].mod == WILD || GetLogo[i].mod == reclogo.mod) && (GetLogo[i].instid == WILD || GetLogo[i].instid == reclogo.instid))
            break;
        }
        
        if (i == nLogo) {
            logit("et", "%s error: logo <%d.%d.%d> not found\n", cmdname, reclogo.instid, reclogo.mod, reclogo.type);
            continue;
        }
        
        
        
        /* Parses line by line till end of message */
        while (getNextTrigger(Wrmsg, triggerLine) != EOF) {
            if (strlen(triggerLine)) { /* Valid SCNL line */
                if (DebugSwitch) { /* If debug enabled ... */
                    logit("o" ,"Got line: %s\n", triggerLine);
                }
                
                if (parseTrigger(triggerLine, &triggerStruct) == 6) { /* Got all parameters, valid line */
                    
                    if (strncmp(triggerStruct.dateStr, "0000", 4) == 0) { /* Invalid date means non-triggered station */
                        /* Force date/time to the one in message header if actual is invalid */
                        dateTime = getDateTime(Wrmsg);
                        buildDateString(dateTime, triggerStruct.dateStr);
                        buildTimeString(dateTime, triggerStruct.timeStr);
                        strcpy(triggerStruct.tag, ""); /* Empty tag */
                        
                        if (DebugSwitch) { /* If debug enabled ... */
                            logit("o" ,"Non triggered station\n");
                        }
                        
                        write = FALSE; /* Do not save to file */
                    }
                    else {
                        strncpy(triggerStruct.tag, TriggeredTag, sizeof(triggerStruct.tag)); /* Fill tag */
                        
                        write = TRUE; /* Save to file */
                    }
                    
                    snprintf(filename, sizeof(filename), "%s_%s.%s", triggerStruct.l, triggerStruct.dateStr, CSVSuffix);
                    
                    /* Write (or append) the line to file */
                    if (write) {
                        buildTrigString(triggerStruct, triggerLine);
                        if (DebugSwitch) { /* If debug enabled ... */
                            logit("o" ,"Parsed as: %s", triggerLine);
                            logit("o" ,"Appending to file: %s\n\n", filename);
                        }
                        if (writeFile(triggerLine, strlen(triggerLine), CSVDir, filename, errText) < 0) {
                            trig2swarm_status(&ErrorLogo, errText);
                        }
                    }
                } //parsed line end
            } //valid line end     
        } //while end
        
    }   /* End of main loop */
}



/********************** Message Stacking Thread *******************
 *           Move messages from transport to memory queue         *
 ******************************************************************/
thr_ret MessageStacker(void *dummy )
{
    long         recsize;	/* size of retrieved message             */
    MSG_LOGO     reclogo;       /* logo of retrieved message             */
    time_t       now;
    char         errText[256]; /* string for log/error/heartbeat messages */
    int          ret;
    int          NumOfTimesQueueLapped= 0; /* number of messages lost due to queue lap */

    /* Tell the main thread we're ok
     ********************************/
    MessageStackerStatus = MSGSTK_ALIVE;

    /* Start main service loop for current connection
     ************************************************/
    while (1) {
        /* Get a message from transport ring
            ************************************/
        ret = tport_getmsg(&InRegion, GetLogo, nLogo, &reclogo, &recsize, Rdmsg, MaxMsgSize);

        switch (ret) {
        case GET_NONE:
            /* Wait if no messages for us */
            sleep_ew(100);
            continue;
            break;
        case GET_TOOBIG:
            time(&now);
            sprintf(errText, "%ld %hd msg[%ld] i%d m%d t%d too long for target", (long)now, (short)ERR_TOOBIG, recsize, (int) reclogo.instid, (int) reclogo.mod, (int)reclogo.type);
            trig2swarm_status(&ErrorLogo, errText);
            continue;
            break;
        case GET_MISS:
            time(&now);
            sprintf(errText, "%ld %hd missed msg(s) i%d m%d t%d in %s", (long)now, (short)ERR_MISSMSG, (int) reclogo.instid, (int) reclogo.mod, (int)reclogo.type, InRing);
            trig2swarm_status(&ErrorLogo, errText);
            break;
        case GET_NOTRACK:
            time(&now);
            sprintf( errText, "%ld %hd no tracking for logo i%d m%d t%d in %s", (long)now, (short)ERR_NOTRACK, (int) reclogo.instid, (int) reclogo.mod, (int)reclogo.type, InRing);
            trig2swarm_status(&ErrorLogo, errText);
            break;
        }

        /* Process retrieved msg (ret==GET_OK,GET_MISS,GET_NOTRACK)
        ***********************************************************/
        Rdmsg[recsize] = '\0';

        /* put it into the 'to be shipped' queue */
        /* the Write thread is in the biz of de-queueng and writing */
        RequestMutex();
        ret = enqueue(&OutQueue, Rdmsg, recsize, reclogo);
        ReleaseMutex_ew();

        switch(ret) {
        case -2:
            /* Serious: quit */
            /* Currently, eneueue() in mem_circ_queue.c never returns this error. */
            time(&now);
            sprintf(errText, "%ld %hd internal queue error. Terminating.", (long)now, (short)ERR_QUEUE);
            trig2swarm_status(&ErrorLogo, errText);
            goto error;
            break;
        case -1:
            time(&now);
            sprintf(errText,"%ld %hd message too big for queue.", (long)now, (short)ERR_QUEUE);
            trig2swarm_status(&ErrorLogo, errText);
            continue;
            break;
        case -3:
            NumOfTimesQueueLapped++;
            if (!(NumOfTimesQueueLapped % 5)) {
                logit("t", "%s(%s): Circular queue lapped 5 times. Messages lost.\n", cmdname, MyModName);
                if (!(NumOfTimesQueueLapped % 100)) {
                    logit("et", "%s(%s): Circular queue lapped 100 times. Messages lost.\n", cmdname, MyModName);
                }
            }
            continue;
        }
    } /* end of while */

    /* we're quitting
     *****************/
error:
    MessageStackerStatus = MSGSTK_ERR; /* file a complaint to the main thread */
    KillSelfThread(); /* main thread will not restart us */
    return THR_NULL_RET; /* Should never get here */
}

/*****************************************************************************
 *  trig2swarm_config() processes command file(s) using kom.c functions;        *
 *                    exits if any errors are encountered.	             *
 *****************************************************************************/
#define NCOMMANDS 11
void trig2swarm_config( char *configfile )
{
    int      ncommand;          /* # of required commands you expect to process   */
    char     init[NCOMMANDS];   /* init flags, one byte for each required command */
    int      nmiss;             /* number of required commands that were missed   */
    char    *com;
    int      nfiles;
    int      success;
    int      i;
    char*    str;

    /* Set to zero one init flag for each required command
     *****************************************************/
    ncommand = NCOMMANDS;
    
    for (i=0; i < ncommand; i++)
        init[i] = 0;
    
    CSVDir = NULL;
    CSVSuffix = NULL;
    TriggeredTag = NULL;
    CSVseparator = NULL;
    CSVSuffix = NULL;
    nLogo = 0;

    /* Open the main configuration file
     **********************************/
    nfiles = k_open(configfile);
    if (nfiles == 0) {
        logit("e" ,"%s: Error opening command file <%s>; exiting!\n", cmdname, configfile);
        exit(-1);
    }

    /* Process all command files
     ***************************/
    while(nfiles > 0) {   /* While there are command files open */
        while(k_rd()) {       /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

            /* Ignore blank lines & comments
                *******************************/
                if(!com)
                    continue;
                if(com[0] == '#')
                    continue;

            /* Open a nested configuration file
                **********************************/
                if(com[0] == '@') {
                    success = nfiles+1;
                    nfiles = k_open(&com[1]);
                    if (nfiles != success) {
                        logit("e" , "%s: Error opening command file <%s>; exiting!\n", cmdname, &com[1]);
                        exit(-1);
                    }
                    continue;
                }

            /* Process anything else as a command
                ************************************/
    /*0*/       if (k_its("LogFile")) {
                    LogSwitch = k_int();
                    init[0] = 1;
                }
                
    /*Optional*/else if (k_its("Debug")) {
                    DebugSwitch = k_int();
                }
                
    /*1*/       else if (k_its("MyModuleId")) {
                    str = k_str();
                    if(str)
                        strcpy( MyModName, str );
                    init[1] = 1;
                }
    /*2*/        else if (k_its("InRing")) {
                    str = k_str();
                    if (str)
                        strcpy( InRing, str );
                    init[2] = 1;
                }
    /*3*/       else if (k_its("HeartBeatInt")) {
                    HeartBeatInt = k_int();
                    init[3] = 1;
                }

            /* Enter installation & module & message types to get
                ****************************************************/
    /*4*/     else if( k_its("GetMsgLogo") ) {
                    if (nLogo >= MAXLOGO) {
                        logit( "e", "%s: Too many <GetMsgLogo> commands in <%s>", cmdname, configfile );
                        logit( "e", "; max=%d; exiting!\n", (int) MAXLOGO );
                        exit( -1 );
                    }
                    if ((str=k_str() ) != NULL) {
                        if (GetInst( str, &GetLogo[nLogo].instid ) != 0) {
                            logit("e" , "%s: Invalid installation name <%s>", cmdname, str);
                            logit("e" , " in <GetMsgLogo> cmd; exiting!\n");
                            exit(-1);
                        }
                    }
                    if ((str=k_str()) != NULL) {
                        if( GetModId( str, &GetLogo[nLogo].mod ) != 0) {
                            logit("e" , "%s: Invalid module name <%s>", cmdname, str);
                            logit("e" , " in <GetMsgLogo> cmd; exiting!\n");
                            exit(-1);
                        }
                    }
                    if ((str=k_str()) != NULL) {
                        if( GetType( str, &GetLogo[nLogo].type ) != 0 ) {
                            logit("e" , "%s: Invalid msgtype <%s>", cmdname, str);
                            logit("e" , " in <GetMsgLogo> cmd; exiting!\n");
                            exit(-1);
                        }
                    }
                    nLogo++;
                    init[4] = 1;
                }

                /* Maximum size (bytes) for incoming/outgoing messages
                    *****************************************************/
        /*5*/   else if (k_its("MaxMsgSize")) {
                        MaxMsgSize = k_long();
                        init[5] = 1;
                    }

                /* Maximum number of messages in outgoing circular buffer
                    ********************************************************/
        /*6*/     else if (k_its("QueueSize")) {
                        QueueSize = k_long();
                        init[6] = 1;
                    }

                /* CSV directory, for creating and writing files
                    ********************************************************/
        /*7*/     else if (k_its("CSVDir")) {
                        if ((str=k_str()) != NULL) {
                            if ((CSVDir = strdup(str)) == NULL) {
                                logit("e", "%s: out of memory for CSVDir\n", cmdname);
                                exit( -1 );
                            }
                        }
                        init[7] = 1;
                    }

                    /* CSV separator
                    ********************************************************/
        /*8*/     else if (k_its("CSVseparator")) {
                        if ((str=k_str()) != NULL) {
                            if ((CSVseparator = strdup(str)) == NULL) {
                                logit("e", "%s: out of memory for CSVseparator\n", cmdname);
                                exit(-1);
                            }
                        }
                        init[8] = 1;
                    }

                    /* CSV suffix
                    ********************************************************/
        /*9*/     else if (k_its("CSVSuffix")) {
                        if ((str=k_str()) != NULL) {
                            if ((CSVSuffix = strdup(str)) == NULL) {
                                logit("e", "%s: out of memory for CSVSuffix\n", cmdname);
                                exit(-1);
                            }
                        }
                        init[9] = 1;
                    }

                    /* Swarm Station triggered tag
                    ********************************************************/
        /*10*/     else if (k_its("TriggeredTag")) {
                        if ((str=k_str()) != NULL) {
                            if ((TriggeredTag = strdup(str)) == NULL) {
                                logit("e" ,"%s: out of memory for TriggeredTag\n", cmdname);
                                exit(-1);
                            }
                        }
                        init[10] = 1;
                    }

                /* Unknown command
                    *****************/
                else {
                        logit("e", "%s: <%s> Unknown command in <%s>.\n", cmdname, com, configfile);
                        continue;
                }

                /* See if there were any errors processing the command
                *****************************************************/
                if (k_err()) {
                    logit("e", "%s: Bad <%s> command  in <%s>; exiting!\n", cmdname, com, configfile);
                    exit(-1);
                }
            }
            nfiles = k_close();
        }

    /* After all files are closed, check init flags for missed commands
     ******************************************************************/
    nmiss = 0;
    for (i = 0; i < ncommand; i++) {
        if(!init[i])
            nmiss++;
    }
    if (nmiss) {
        logit("e", "%s: ERROR, no ", cmdname);
        if (!init[0])
            logit("e", "<LogFile> ");
        if (!init[1])
            logit("e", "<MyModuleId> ");
        if (!init[2])
            logit("e", "<InRing> ");
        if (!init[3])
            logit("e", "<HeartBeatInt> ");
        if (!init[4])
            logit("e", "<GetMsgLogo> ");
        if (!init[5])
            logit("e", "<MaxMsgSize> ");
        if (!init[6])
            logit("e", "<Queue>");
        if (!init[7])
            logit("e", "<CSVDir>");
        if (!init[8])
            logit("e", "<CSVseparator>");
        if (!init[9])
            logit("e", "<CSVSuffix>");
        if (!init[10])
            logit("e", "<TriggeredTag>");
        
        logit("e", "command(s) in <%s>; exiting!\n", configfile);
        exit(-1);
    }

    /* Make sure our directories exist */
    if (RecursiveCreateDir(CSVDir) != EW_SUCCESS) {
        logit("e", "%s error creating CSVDir: %s\n", cmdname, strerror(errno));
        exit(-1);
    }

    return;
}

/****************************************************************************
 *  trig2swarm_lookup( )   Look up important info from earthworm.h tables      *
 ****************************************************************************/
void trig2swarm_lookup(void)
{
    /* Look up keys to shared memory regions
     *************************************/
    if ((InRingKey = GetKey(InRing)) == -1 ) {
        logit("e", "%s:  Invalid ring name <%s>; exiting!\n", cmdname, InRing);
        exit(-1);
    }

    /* Look up installations of interest
     *********************************/
    if (GetLocalInst( &HeartLogo.instid ) != 0) {
        logit("e", "%s: error getting local installation id; exiting!\n", cmdname);
        exit(-1);
    }
    ErrorLogo.instid = HeartLogo.instid;

    /* Look up modules of interest
     ***************************/
    if (GetModId(MyModName, &HeartLogo.mod) != 0) {
        logit("e", "%s: Invalid module name <%s>; exiting!\n", cmdname, MyModName);
        exit(-1);
    }
    ErrorLogo.mod = HeartLogo.mod;

    /* Look up message types of interest
     *********************************/
    if (GetType("TYPE_HEARTBEAT", &HeartLogo.type ) != 0) {
        logit("e", "%s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n", cmdname);
        exit(-1);
    }
    if (GetType("TYPE_ERROR", &ErrorLogo.type ) != 0) {
        logit("e", "%s: Invalid message type <TYPE_ERROR>; exiting!\n", cmdname);
        exit(-1);
    }
    TypeError = ErrorLogo.type;

    if (GetType("TYPE_TRIGLIST_SCNL", &TypeTriglistSCNL ) != 0) {
        logit("e", "%s: Invalid message type <TYPE_TRIGLIST_SCNL>; exiting!\n", cmdname);
        exit(-1);
    }

    return;
}

/***************************************************************************
 * trig2swarm_status() sends an error or heartbeat message to transport    *
 *    If the message is of TYPE_ERROR, the text will also be logged.       *
 *    Since trig2swarm_status is called be more than one thread, the logo  *
 *    and message must be constructed by the caller.                       *
 ***************************************************************************/
void trig2swarm_status(MSG_LOGO *logo, char *msg)
{
    size_t size;

    if (logo->type == TypeError)
        logit("et", "%s\n", msg);

    size = strlen(msg);   /* don't include the null byte in the message */

    /* Write the message to shared memory
     ************************************/
    if (tport_putmsg(&InRegion, logo, (long)size, msg) != PUT_OK) {
        logit("et", "%s(%s):  Error sending message to transport.\n", cmdname, MyModName);
    }
    
    return;
}


/* the function to write string to a named file
 * returns 0 upon success or -1 and sets errText upon failure
*/
int writeFile(char *msg, size_t msglen, char *dir, char *filename, char *errText)
{
    char outFile[MAXPATHLEN];
    time_t now;
    int ret;
    FILE *fp;
    char err[64];

    sprintf(outFile,  "%s/%s", dir, filename);
    
    fp = fopen(outFile, "ab");
    if (fp == NULL) {
        strcpy(err, "error creating file");
        goto error;
    }
    
    ret = (int)fwrite(msg, msglen, 1, fp);
    if (ret != 1) {
        strcpy(err, "error writing file");
        fclose(fp);
        goto error;
    }
    
    if (fclose(fp) != 0) {
        strcpy(err, "error completing file");
        goto error;
    }
    return 0;
    
error:
    time(&now);
    sprintf(errText, "%ld %hd %s %s: %s", (long)now, (short)ERR_WRITE, err, outFile, strerror(errno));
    return -1;
}






/* Build string based on date */
int buildDateString(struct tm dateTime, char *tstring)
{
    const char empty[] = "0000-00-00";
    int retval = 0;
    
    if (tstring != NULL) {
        if (dateTime.tm_year == 0) /* build empty string */
            strcpy(tstring, empty);
        else /* build final-format string */
            sprintf(tstring, "%04u-%02u-%02u", dateTime.tm_year+1900, dateTime.tm_mon+1, dateTime.tm_mday);
        retval++;
    }
    
    return retval;
}


/* Build string based on time */
int buildTimeString(struct tm dateTime, char *tstring)
{
    int retval = 0;
    
    if (tstring != NULL) {
        /* build final-format string */
        sprintf(tstring, "%02u:%02u:%02u", dateTime.tm_hour, dateTime.tm_min, dateTime.tm_sec);
        retval++;
    }
    
    return retval;
}


/* return pointer to date YYYYMMDD */
char * matchDate(char *msg)
{
    regex_t reegex;
    regmatch_t rm;
 
    if (msg != NULL) {
        if (regcomp(&reegex, "\\b[0-9]{8}\\b", REG_EXTENDED) == 0) {
            if (regexec(&reegex, msg, 1, &rm, 0) == 0) { /* Match */
    //             printf("Match date\n");
                return (msg + rm.rm_so);
            }
        }
    }
 
    return NULL;
}

/* return pointer to time HH:mm:ss */
char * matchTime(char *msg)
{
    regex_t reegex;
    regmatch_t rm;
 
    if (msg != NULL) {
        if (regcomp(&reegex, "\\b[0-9]{2}:[0-9]{2}:[0-9]{2}\\b", REG_EXTENDED) == 0) {
            if (regexec(&reegex, msg, 1, &rm, 0) == 0) { /* Match */
    //             printf("Match time\n");
                return (msg + rm.rm_so);
            }
        }
    }
 
    return NULL;
}

/* return pointer to date time YYYYMMDD HH:mm:ss */
char * matchDateTime(char *msg)
{
    regex_t reegex;
    regmatch_t rm;
    
    if (msg != NULL) {
        if (regcomp(&reegex, "\\b[0-9]{8} [0-9]{2}:[0-9]{2}:[0-9]{2}\\b", REG_EXTENDED) == 0) {
            if (regexec(&reegex, msg, 1, &rm, 0) == 0) { /* Match */
    //             printf("Match dateTime\n");
                return (msg + rm.rm_so);
            }
        }
    }
 
    return NULL;
}

/* get date from string */
void getDate(char *msg, struct tm *dateTime)
{
    int year = 1900;
    int month = 1;
    int day = 0;

 
    if (msg != NULL) { /* Error check */
        sscanf(msg, "%4d%2d%2d", &year, &month, &day);
    }
    
    dateTime->tm_year = year - 1900;
    dateTime->tm_mon = month - 1;
    dateTime->tm_mday = day;

}

/* get time from string */
void getTime(char *msg, struct tm *dateTime)
{
    if (msg != NULL) { /* Error check */
       sscanf(msg, "%2d:%2d:%2d", &dateTime->tm_hour, &dateTime->tm_min, &dateTime->tm_sec);
    }
}

/* get date and time from string */
struct tm getDateTime(char *msg)
{
    struct tm dateTime = {0};
    
    if (msg != NULL) { 
        char *ptr = matchDateTime(msg);
        
        if (ptr != NULL) {
            getDate(matchDate(ptr), &dateTime);
            getTime(matchTime(ptr), &dateTime);
    //         printf("Get dateTime\n");
        }
    }
    return dateTime;
}


/* Parses a line and saves data into struct
 * 
 * Returns zero if could not parse data
 */
int parseTrigger(char *line, TrigLine *trig)
{
    char *endField, *nextField;
    int retval = 0;
        
    /* Error check */
    if (trig == NULL || line == NULL)
        return retval;
    
    /* Clear strings */
    trig->s[0] = '\0';
    trig->c[0] = '\0';
    trig->n[0] = '\0';
    trig->l[0] = '\0';
    trig->dateStr[0] = '\0';
    trig->timeStr[0] = '\0';
    
    
    /* Position zero */
    nextField = strtok_r(line, " ", &endField);
    if (nextField == NULL)
        return retval;
    
    strncpy(trig->s, nextField, sizeof(trig->s));
    if (strlen(trig->s) < 3) /* Check station name is long enough */
        return retval;
    else
        retval++;
    
    nextField = strtok_r(NULL, " ", &endField);
    if (nextField == NULL)
        return retval;
    
    strncpy(trig->c, nextField, sizeof(trig->c));
    if (strlen(trig->c) < 3) /* Check channel is long enough */
        return retval;
    else
        retval++;
   
    nextField = strtok_r(NULL, " ", &endField);
    if (nextField == NULL)
        return retval;
    
    strncpy(trig->n, nextField, sizeof(trig->n));
    retval++;
    
    nextField = strtok_r(NULL, " ", &endField);
    if (nextField == NULL)
        return retval;
    
    strncpy(trig->l, nextField, sizeof(trig->l));
    retval++;
    
    /* Discard dummy field */
    nextField = strtok_r(NULL, " ", &endField);
    
    /* Get date field */
    struct tm dateTime;
    nextField = strtok_r(NULL, " ", &endField);
    if (nextField == NULL)
        return retval;
    
    getDate(nextField, &dateTime);
    retval += buildDateString(dateTime, trig->dateStr);
    
    
    /* Get time */
    nextField = strtok_r(NULL, " ", &endField);
    if (nextField == NULL)
        return retval;
    
    getTime(nextField, &dateTime);
    retval += buildTimeString(dateTime, trig->timeStr);
    
    return retval;
}



/* Returns next valid trigger line.
 * 
 * Returns 0 if no valid line found
 * Returns EOF when message reached to end
 */
int getNextTrigger(char *msg, char *line)
{
    char *nextLine;
    int retval = EOF;
    static char *endLine = NULL;
    
    /* Error check */
    if (msg == NULL || line == NULL)
        return retval;
        
    /* Empty string */
    line[0] = '\0';
    
    /* Position zero */
    if (endLine == NULL) {/* First run */
        nextLine = strtok_r(msg, "\n", &endLine);
    }
    else {
        nextLine = strtok_r(NULL, "\n", &endLine);
    }
    
    /* Found new line */
    if (nextLine != NULL) {
        if (nextLine[0] == ' ') { /* Valid SCNL line start with space, proceed */
            nextLine++; /* Skip space */
            strcpy(line, nextLine);
            retval = strlen(line);
        }
        else {
            retval = 0;
        }
    }
    else { /* EOF, reset pointer */
        endLine = NULL;
    }
    
    
    return retval;
}


/* Build trigger string according to Swarm
 * tag format:
 * 
 * YYYY-mm-dd HH:mm:ss ,  SSSS CCC NN LL ,  tag
 */
int buildTrigString(TrigLine trig, char *buffer)
{
    int bytesWritten = 0;
    
    /* Write first field */
    bytesWritten += sprintf(buffer + bytesWritten, "%s %s", trig.dateStr, trig.timeStr);
    
    /* Write separator */
    bytesWritten += sprintf(buffer + bytesWritten, "%s", CSVseparator);
    
    /* Write second field */
    bytesWritten += sprintf(buffer + bytesWritten, "%s %s %s %s", trig.s, trig.c, trig.n, trig.l);
    
    /* Write separator */
    bytesWritten += sprintf(buffer + bytesWritten, "%s", CSVseparator);
    
    /* Write third field */
    bytesWritten += sprintf(buffer + bytesWritten, "%s\n", trig.tag);
    
    return bytesWritten;
}

