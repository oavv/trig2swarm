#   trig2swarm configuration file
#
#
MyModuleId     MOD_TRIG2SWARM      # module id for this program
InRing         HYPO_RING           # transport ring to use for input
HeartBeatInt   30                  # EW internal heartbeat interval (sec)
LogFile        ${EW_LOG_MODE}      # If 0, don't write logfile
                                   # if 2, write to module log but not to
                                   # stderr/stdout.
# Set to 1 to debug to log file
Debug   0
#
# Logo of messages to read
# Each message of a given logo will be written to a CSV file
# DO NOT change module / message type
#              Installation       Module            Message Type
GetMsgLogo    ${EW_INST_ID}      MOD_CARLSUBTRIG   TYPE_TRIGLIST_SCNL

MaxMsgSize        16384      # maximum size (bytes) for input msgs
QueueSize         100        # number of msgs to buffer

#CSVDir: Directory to which output CSV files will be written.
CSVDir      "${EW_DATA_DIR}/swarm"

#CSVseparator   ";"
#CSVseparator   " "
CSVseparator   ","

#File will be named according to following rule:
#Network_YYYY-mm-dd.csv
CSVSuffix      "csv"

#How to tag triggers so Swarm will read them
TriggeredTag       "EW_TRIG"
